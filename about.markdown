---
layout: page
title: About
permalink: /about/
---

Youtube videos of old baseball games for your viewing pleasure while the MLB (and all US sports) are suspended due to the COVID-19 outbreak. 

## Frequently Asked Questions

### _Why?_

Coronavirus is scary, baseball is fun.

### Why are there so few seasons?

There are games from seasons not shown in the search form, just not many. It wouldn't make sense to show a season with three games in the search bar, 24/30 searches against it would have no results. 

### Why aren't there more games for my team!?

I'm only indexing videos I'm able to find. [Email](mailto:coronavirusbaseball@gmail.com) me Youtube links for your team and I'll add them.

### I want more games!

Me too! I'm manually scraping these videos as of now thanks to Youtube's draconian API limits. I hope to add more games daily.