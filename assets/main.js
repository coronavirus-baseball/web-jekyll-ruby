covid_19 = {
    API_URL: 'https://api.coronavirus-baseball.com/videos',
    LOADER_HTML: '<div class="spinner-wrapper"><div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>',
    init: function() {
        console.log('Application init');

        $.ajax({
            url: '/assets/data/teams/teams.json',
            success: function(data, status, jqxhr) {
                var teams_list = Array();
                for (var i in data['teams']) {
                    team = data['teams'][i];
                    team['id'] = i;
                    teams_list.push(team);
                }
                teams_list.sort(covid_19.team_compare);

                for(var i in teams_list) {
                    var team = teams_list[i];
                    $('#select_team').append('<option value="' + team['id'] + '">' + team['name'] + '</option>');
                }
            }
        });

        $.ajax({
            url: '/assets/data/seasons/seasons.json',
            success: function(data, status, jqxhr) {
                for (var i in data['seasons']) {
                    $('#select_season').append('<option value="' + i + '">' + i + '</option>');
                }
            }
        });

        $('#api_search').on('submit', covid_19.api_request);
    },
    api_request: function() {
        console.log('API request');
        $('#embed_container').html(covid_19.LOADER_HTML);

        if(typeof ga !== 'undefined') {
            ga('send', {
                'hitType': 'event',
                'eventCategory': 'api-request',
                'eventAction': 'request',
                'hitCallback': function () {
                    console.log("Google Analytics — event sent");
                }
            });

            if ($('#select_team').val() != '') {
                ga('send', {
                    'hitType': 'event',
                    'eventCategory': 'api-request',
                    'eventAction': 'team',
                    'eventLabel': $('#select_team').val()
                });
            }

            if ($('#select_season').val() != '') {
                ga('send', {
                    'hitType': 'event',
                    'eventCategory': 'api-request',
                    'eventAction': 'season',
                    'eventLabel': $('#select_season').val()
                });
            }
        }

        $.ajax({
            url: covid_19.API_URL + '?team=' + $('#select_team').val() + '&season=' + $('#select_season').val(),
            success: function(data, textStatus, jqXHR) {
                if(data['videos'].length > 0) {
                    var video = data['videos'][Math.floor(Math.random() * data['videos'].length)];
                    var embed_iframe = '<div class="video-container"><iframe width="740" height="416" src="https://www.youtube.com/embed/' + video['id'] + '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>';
                    $('#embed_container').html(embed_iframe);
                } else {
                    var error_html = '<div class="error"><i class="fa fa-times-circle"></i> No results found.</div>';
                    $('#embed_container').html(error_html);
                }
            },
            error: function(jqXHR, textStatus, errorThrown ) {
                console.log(errorThrown);
                var error_html = '<div class="error"><i class="fa fa-times-circle"></i> Error thrown: ' + errorThrown + '</div>';
                $('#embed_container').html(error_html);
            }
        });
        return false;
    },
    team_compare: function(a, b) {
        const a_name = a.name.toUpperCase();
        const b_name = b.name.toUpperCase();
        
        let comparison = 0;
        if (a_name > b_name) {
            comparison = 1;
        } else if (a_name < b_name) {
            comparison = -1;
        }
        return comparison;
    },
};

 $(document).ready(covid_19.init());
